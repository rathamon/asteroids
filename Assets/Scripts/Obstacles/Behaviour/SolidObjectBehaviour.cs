﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class SolidObjectBehaviour : MonoBehaviour, IPausable
{    
    [SerializeField] private GameObject _asteroidPrefab;
    [SerializeField] private GameObject _misslePrefab;
    [SerializeField] private bool _autoFillShips = false;
    public ShipAttributeAssociation[] Ships;

    private LevelPlayer _gameLogic;
    private Vector3 _playerPosition;
    private Vector2 _screenBounds;
    private ISolidObjectSpawner _spawner;

    private bool _isPaused = false;

    private List<SolidObject> _enemies = new List<SolidObject>();
    private List<SolidObject> _asteroids = new List<SolidObject>();
    private List<SolidObject> _missles = new List<SolidObject>();
    

    #region Monobehaviour
    void OnValidate()
    {
        if (_autoFillShips)
        {
            string[] shipAssets = System.Enum.GetNames(typeof(EnemyType));
            Ships = new ShipAttributeAssociation[3];
            for (int i = 0; i < shipAssets.Length; i++)
            {
                Ships[i] = new ShipAttributeAssociation
                {
                    type = (EnemyType)System.Enum.ToObject(typeof(EnemyType), i),
                    shipAsset = Resources.FindObjectsOfTypeAll<EnemyShipAsset>().First(enemyShipAsset => { return enemyShipAsset.name == shipAssets[i]; })
                };
            }
        }
    }
    #endregion

    void Start()
    {
        _gameLogic = GetComponent<LevelPlayer>();

        _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

        Dictionary<EnemyType, ShipAttributeAssociation> shipPrefabs = new Dictionary<EnemyType, ShipAttributeAssociation>();
        for (int i = 0; i < Ships.Length; i++)
        {
            shipPrefabs.Add(Ships[i].type, Ships[i]);
        }

        if (_spawner == null)
        {
            _spawner = new SolidObjectSpawner();
            _spawner.SetAsteroidPrefab(_asteroidPrefab);
            _spawner.SetMisslePrefab(_misslePrefab);
            _spawner.SetEnemyPrefabs(shipPrefabs);
        }
    }
    void Update()
    {
        if (_isPaused)
            return;

        float time = Time.deltaTime;
        void DestroySolid(ref List<SolidObject> solidList, ref int index, SolidType solidType)
        {
            _spawner.Destroy(solidList[index], solidType);
            solidList.RemoveAt(index);
            index--;
        }

        for (int i=0;i<_asteroids.Count;i++)
        {
            if (_asteroids[i].Health <= 0)
            {
                _gameLogic.Points += _asteroids[i].Points;
                DestroySolid(ref _asteroids,ref i, SolidType.Asteroid);
                continue;
            }

            if (_asteroids[i].transform.position.y < -_screenBounds.y)
            {
                DestroySolid(ref _asteroids,ref i, SolidType.Asteroid);
                continue;
            }
            _asteroids[i].transform.position += (Vector3)_asteroids[i].Velocity * time;
        }
   

        for (int i = 0; i < _missles.Count; i++)
        {
            Vector3 currentMisslePositon = _missles[i].transform.position;            

            if (_missles[i].Health <= 0 || currentMisslePositon.y < -_screenBounds.y || currentMisslePositon.y > _screenBounds.y)
            {
                DestroySolid(ref _missles,ref i, SolidType.Missle);
                continue;
            }

            _missles[i].transform.position = currentMisslePositon + (Vector3)_missles[i].Velocity * time;            
        }

        for (int i = 0; i < _enemies.Count; i++)
        {
            Vector3 currentEnemyPosition = _enemies[i].transform.position;

            _enemies[i].transform.up = _playerPosition - currentEnemyPosition;
            _enemies[i].transform.position = currentEnemyPosition + (Vector3)_enemies[i].Velocity * time;

            if (_enemies[i] is EnemyShip enemy)
            {
                enemy.CurrentGunCoolingTime += time;
                if (enemy.CurrentGunCoolingTime > enemy.GunCoolingTime)
                {
                    enemy.CurrentGunCoolingTime = 0;
                    foreach (Transform gun in enemy.Guns)
                    {
                        SpawnMissle(gun.transform.position, gun.transform.rotation, true, enemy.MissleSpeed, enemy.MissleDamage);
                    }
                }
                if (currentEnemyPosition.y < 0 && !enemy.IsEscaping)
                {
                    enemy.IsEscaping = true;
                    float sideVelocity = enemy.MissleSpeed;
                    if (Random.Range(0, 1) == 1)
                    {
                        sideVelocity *= -1;
                    }
                    enemy.Velocity = new Vector2(sideVelocity, 0);
                }
            }                

            if (_enemies[i].Health <= 0)
            {
                _gameLogic.Points += _enemies[i].Points;
                DestroySolid(ref _enemies,ref i, SolidType.EnemyShip);
                continue;
            }

            if (currentEnemyPosition.x < -_screenBounds.x || currentEnemyPosition.x > _screenBounds.x)
            {
                DestroySolid(ref _enemies,ref i, SolidType.EnemyShip);
                continue;
            }            
        }        
    }

    public void SpawnMissle(Vector3 position, Quaternion rotation, bool isEnemy, float speed, float damage)
    {
        _missles.Add(_spawner.SpawnMissle(position, rotation, isEnemy, speed, damage));
    }
    public void SpawnAsteroid(Vector3 position, Vector2 speed, float size)
    {
        _asteroids.Add(_spawner.SpawnAsteroid(position, speed, size));
    }
    public void SpawnEnemy(Vector3 position, EnemyType type)
    {
        _enemies.Add(_spawner.SpawnEnemy(position,type));
    }

    public void DestroyAll()
    {
        _asteroids.ForEach(Asteroid =>
       {
           _spawner.Destroy(Asteroid, SolidType.Asteroid);
       });

        _missles.ForEach(Missle =>
       {
           _spawner.Destroy(Missle, SolidType.Missle);
       });

        _enemies.ForEach(Enemy =>
       {
           _spawner.Destroy(Enemy, SolidType.EnemyShip);
       });

        _enemies.Clear();
        _asteroids.Clear();
        _missles.Clear();
    }
    public void SetPlayerPosition(Vector3 position)
    {
        _playerPosition = position;
    }
    public void SetPauseState(bool isPaused)
    {
        _isPaused = isPaused;

    }
}

[System.Serializable]
public struct ShipAttributeAssociation
{
    public EnemyType type;
    public EnemyShipAsset shipAsset;
}
public enum Layer : int
{
    Player = 8,
    Enemy = 9,
    PlayerMissle = 10,
    EnemyMissle = 11
}
public enum SolidType
{
   EnemyShip,
   Asteroid,
   Missle
}


