﻿using UnityEngine;
using System.Collections.Generic;
public interface ISolidObjectSpawner
{
    void SetAsteroidPrefab(GameObject prefab);
    SolidObject SpawnAsteroid(Vector3 position, Vector2 speed, float size);
    void SetEnemyPrefabs(IDictionary<EnemyType, ShipAttributeAssociation> prefabs);
    SolidObject SpawnEnemy(Vector3 position, EnemyType type);
    void SetMisslePrefab(GameObject prefab);
    SolidObject SpawnMissle(Vector3 position, Quaternion rotation, bool isEnemy, float speed, float damage);
    void Destroy(SolidObject destroyedObject, SolidType solidType);
}
