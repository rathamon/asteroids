﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class SolidObjectSpawner : ISolidObjectSpawner
{
    private List<EnemyShip> _destroyedEnemies = new List<EnemyShip>();
    private Stack<SolidObject> _destroyedAsteroids = new Stack<SolidObject>();
    private Stack<SolidObject> _destroyedMissles = new Stack<SolidObject>();
    private GameObject _asteroidPrefab;
    private GameObject _misslePrefab;
    private IDictionary<EnemyType, ShipAttributeAssociation> _enemyPrefabs;

    public void Destroy(SolidObject destroyedObject, SolidType solidType)
    {
        switch (solidType)
        {
            case SolidType.EnemyShip:
                DestroyEnemy(destroyedObject);
                break;
            case SolidType.Asteroid:
                DestroyAsteroid(destroyedObject);
                break;
            case SolidType.Missle:
                DestroyMissle(destroyedObject);
                break;
        }
    }

    private void DestroyAsteroid(SolidObject asteroid)
    {
        asteroid.gameObject.SetActive(false);
        _destroyedAsteroids.Push(asteroid);
    }

    private void DestroyEnemy(SolidObject enemy)
    {
        enemy.gameObject.SetActive(false);
        if (enemy is EnemyShip enemyShip)
        {
            _destroyedEnemies.Add(enemyShip);
        }
    }
    private void DestroyMissle(SolidObject missle)
    {
        missle.gameObject.SetActive(false);
        _destroyedMissles.Push(missle);
    }
    public void SetAsteroidPrefab(GameObject prefab)
    {
        _asteroidPrefab = prefab;
    }
    public void SetEnemyPrefabs(IDictionary<EnemyType, ShipAttributeAssociation> prefabs)
    {
        _enemyPrefabs = prefabs;
    }
    public void SetMisslePrefab(GameObject prefab)
    {
        _misslePrefab = prefab;
    }
    public SolidObject SpawnAsteroid(Vector3 position, Vector2 speed, float size)
    {
        SolidObject asteroidBodyComponent;
        if (_destroyedAsteroids.Count == 0)
        {
            GameObject newAsteroid = GameObject.Instantiate(_asteroidPrefab, position, new Quaternion());
            asteroidBodyComponent = newAsteroid.GetComponent<SolidObject>();
        }
        else
        {
            asteroidBodyComponent = _destroyedAsteroids.Pop();
            asteroidBodyComponent.transform.position = position;
            asteroidBodyComponent.transform.rotation = new Quaternion();
            asteroidBodyComponent.gameObject.SetActive(true);
        }

        asteroidBodyComponent.gameObject.layer = (int)Layer.Enemy;
        asteroidBodyComponent.SetSolidObjectAttributes(size * 100, size * 100, size * 100, speed);
        asteroidBodyComponent.transform.localScale = new Vector3(size, size, 1);

        return asteroidBodyComponent;
    }
    public SolidObject SpawnEnemy(Vector3 position, EnemyType type)
    {
        EnemyShip enemyBodyComponent = _destroyedEnemies.Find(Enemy =>
        {
            return Enemy.Type == type;
        });

        ShipAttributeAssociation newShip = _enemyPrefabs[type];
        if (enemyBodyComponent == null)
        {
            GameObject newEnemy = GameObject.Instantiate(newShip.shipAsset.prefab, position, new Quaternion());
            enemyBodyComponent = newEnemy.GetComponent<EnemyShip>();
        }
        else
        {
            enemyBodyComponent.transform.position = position;
            enemyBodyComponent.transform.rotation = Quaternion.identity;
            enemyBodyComponent.gameObject.SetActive(true);
        }

        enemyBodyComponent.gameObject.layer = (int)Layer.Enemy;
        enemyBodyComponent.SetShipAttributes(newShip.shipAsset.attributes);

        return enemyBodyComponent;
    }
    public SolidObject SpawnMissle(Vector3 position, Quaternion rotation, bool isEnemy, float speed, float damage)
    {
        SolidObject missleBodyComponent;

        if (_destroyedMissles.Count == 0)
        {
            GameObject newMissle = GameObject.Instantiate(_misslePrefab, position, rotation);
            missleBodyComponent = newMissle.GetComponent<SolidObject>();
        }
        else
        {
            missleBodyComponent = _destroyedMissles.Pop();
            missleBodyComponent.transform.position = position;
            missleBodyComponent.transform.rotation = rotation;
            missleBodyComponent.gameObject.SetActive(true);
        }

        if (isEnemy)
        {
            missleBodyComponent.gameObject.layer = (int)Layer.EnemyMissle;
        }
        else
        {
            missleBodyComponent.gameObject.layer = (int)Layer.PlayerMissle;
        }

        missleBodyComponent.SetSolidObjectAttributes(1, 0, damage, (rotation * Vector3.up) * speed);

        return missleBodyComponent;
    }
}
