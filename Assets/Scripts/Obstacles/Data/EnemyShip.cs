﻿using UnityEngine;
public class EnemyShip : SolidObject
{
    public float GunCoolingTime;
    public float CurrentGunCoolingTime;
    public float MissleSpeed;
    public float MissleDamage;
    public bool IsEscaping;
    public Transform[] Guns;
    public readonly EnemyType Type;
    public void SetShipAttributes(ShipAttributes shipAttributes)
    {
        GunCoolingTime = shipAttributes.GunCoolingTime;
        MissleSpeed = shipAttributes.MissleSpeed;
        MissleDamage = shipAttributes.MissleDamage;
        Health = shipAttributes.ShipHealth;
        Points = shipAttributes.ShipPoints;
        Damage = MissleDamage;
        Velocity = new Vector2(0, -shipAttributes.ShipSpeed);
        IsEscaping = false;
    }
}

[System.Serializable]
public struct ShipAttributes
{
    public float GunCoolingTime;
    public float MissleSpeed;
    public float MissleDamage;
    public float ShipSpeed;
    public float ShipHealth;
    public float ShipPoints;
}
