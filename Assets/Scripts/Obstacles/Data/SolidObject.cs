﻿using UnityEngine;
public class SolidObject : MonoBehaviour
{
    public Vector2 Velocity;
    public float Damage;
    public float Health;
    public float Points;
    public void SetSolidObjectAttributes(float health, float points, float damage, Vector2 velocity)
    {
        Health = health;
        Points = points;
        Damage = damage;
        Velocity = velocity;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        SolidObject body = other.GetComponent<SolidObject>();
        Health -= body.Damage;
    }     
}
