﻿using UnityEngine;
public class PlayerControl
{
    private float _yPosition = 0;
    private float _zPosition = 0;
    private Vector2 _screenBounds;
    public PlayerControl(Vector3 playerPosition)
    {
        _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        _yPosition = playerPosition.y;
        _zPosition = playerPosition.z;
    }
    public Vector3 GetPlayerPosition()
    {
        Vector3 newPlayerPosition = Input.mousePosition;
        float xPosition = 2f * Mathf.Clamp(Input.mousePosition.x / Screen.width, 0, 1f) - 1f;
        return new Vector3(xPosition * _screenBounds.x, _yPosition, _zPosition);
    }
}
