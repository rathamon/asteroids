﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="New Enemy Ship", menuName = "Asteroids/Enemy Ship")]
public class EnemyShipAsset : ScriptableObject
{
    public GameObject prefab;
    public ShipAttributes attributes;
}
