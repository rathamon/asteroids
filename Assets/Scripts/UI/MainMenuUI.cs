﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MainMenuUI : MonoBehaviour
{
    [SerializeField] private string levelName = "Game";
    [SerializeField] private Text GameLabelText;
    [SerializeField] private Text StartGameText;
    [SerializeField] private Text ExitGameText;
    void Start()
    {
        UpdateButtonText("Астероиды", "Начать игру", "Выход");
    }
    void UpdateButtonText(string gameLabel, string startGame, string exitGame)
    {
        GameLabelText.text = gameLabel;
        StartGameText.text = startGame;
        ExitGameText.text = exitGame;
    }
    public void StartGame()
    {
        SceneManager.LoadScene(levelName);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
