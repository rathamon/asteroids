﻿public interface IUserInterface
{
    void ShowEndgameMenu(string points);
    void ShowPauseMenu();
    void HidePauseMenu();
    void SetCurrentPlayerStats(string health, string points);
}