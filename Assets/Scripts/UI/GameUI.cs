﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameUI : MonoBehaviour, IUserInterface, IPausable
{
    [SerializeField] private string _gameSceneName = "Game";
    [SerializeField] private string _mainMenuSceneName = "MainMenu";
    [SerializeField] private Text _pointsText;
    [SerializeField] private Text _healthText;
    [SerializeField] private Text _pauseButtonText;
    [SerializeField] private Text _healthAmountText;
    [SerializeField] private Text _pointsAmountText;
    [SerializeField] private GameObject _pauseMenu;
    [SerializeField] private Text _pauseMenuText;
    [SerializeField] private Text _resumeButtonText;
    [SerializeField] private Text _mainMenuButtonText;
    [SerializeField] private Text _exitButtonText;

    void Start()
    {
        UpdateButtonText("Очки:", "Жизни:", "Пауза", "Возобновить", "В главное меню", "Выход из игры", "Игра приостановлена");
    }
    public void SetCurrentPlayerStats(string health, string points)
    {
        _pointsAmountText.text = points;
        _healthAmountText.text = health;
    }
    void UpdateButtonText(string pointsText, string healthText, string pauseButtonText, string resumeButtonText, string mainMenuButtonText, string exitButtonText, string pauseMenuText)
    {
        _pointsText.text = pointsText;
        _healthText.text = healthText;
        _pauseButtonText.text = pauseButtonText;
        _resumeButtonText.text = resumeButtonText;
        _mainMenuButtonText.text = mainMenuButtonText;
        _exitButtonText.text = exitButtonText;
        _pauseMenuText.text = pauseMenuText;
    }
    public void ShowEndgameMenu(string points)
    {
        UpdateButtonText("Очки:", "Жизни:", "Пауза", "Новая игра", "В главное меню", "Выход из игры", "Ваш счет: " + points);
        _pauseMenu.SetActive(true);
    }
    public void ShowPauseMenu()
    {
        UpdateButtonText("Очки:", "Жизни:", "Пауза", "Возобновить", "В главное меню", "Выход из игры", "Игра приостановлена");
        _pauseMenu.SetActive(true);
    }
    public void HidePauseMenu()
    {
        _pauseMenu.SetActive(false);
    }
    public void GoToMainMenu()
    {
        SceneManager.UnloadSceneAsync(_gameSceneName);
        SceneManager.LoadScene(_mainMenuSceneName);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void SetPauseState(bool isPaused)
    {
        if (isPaused)
        {
            ShowPauseMenu();
        }
        else
        {
            HidePauseMenu();           
        }
    }
}
