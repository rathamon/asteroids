﻿using UnityEngine;
using System.Linq;
using UnityEngine.Events;

public class LevelPlayer : MonoBehaviour, IPausable
{
    public UnityEvent<bool> PauseEvent;

    [SerializeField] private bool _isPaused = false;
    [SerializeField] private string _numbersFormatStyle = "0";

    [SerializeField] private SolidObjectBehaviour _solidObjectBehaviour;
    [SerializeField] private IUserInterface _userInterface;
    [SerializeField] private SolidObject _player;
    [SerializeField] private PlayerControl _playerControl;
    [SerializeField] private ObstacleSpawner _obstacleSpawner;

    [SerializeField] private float _playerFireTime = 0.2f;
    [SerializeField] private float _playerFireTimeElapsed = 0;
    [SerializeField] private float _playerMaxHealth = 1000;
    [SerializeField] private float _playerMissleSpeed = 40f;
    [SerializeField] private int _playerMissleDamage = 50;
    public float Points { get; set; }

    private float _health;
    public float Health
    {
        get => _health;
        set
        {
            _player.Health = value;
            _health = value;
        }
    }

    public void StartNewGame()
    {
        PauseGame(false);
    }
    void Start()
    {
        Vector2 screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        if (_solidObjectBehaviour == null)
        {
            _solidObjectBehaviour = GetComponent<SolidObjectBehaviour>();
        }
        _obstacleSpawner = new ObstacleSpawner(screenBounds, ref _solidObjectBehaviour);

        _player = GameObject.Find("Player").GetComponent<SolidObject>();
        _playerControl = new PlayerControl(_player.transform.position);
        _player.Health = _playerMaxHealth;
        _userInterface = FindObjectsOfType<MonoBehaviour>().OfType<IUserInterface>().First();

        if (PauseEvent == null)
            PauseEvent = new UnityEvent_bool();

        FindObjectsOfType<MonoBehaviour>().OfType<IPausable>().ToList().ForEach(PausableScript =>
        {
           PauseEvent.AddListener(PausableScript.SetPauseState);
        });
    }

    void Update()
    {
        if (_isPaused)
            return;

        if (_player.Health <= 0)
        {
            EndGame();
        }
        else
        {
            Health = _player.Health;
        }

        _obstacleSpawner.Update();

        _playerFireTimeElapsed += Time.deltaTime;
        if (_playerFireTimeElapsed >= _playerFireTime)
        {
            _playerFireTimeElapsed = 0;
            PlayerFire();
        }

        _player.transform.position = _playerControl.GetPlayerPosition();
        _solidObjectBehaviour.SetPlayerPosition(_player.transform.position);
        _userInterface.SetCurrentPlayerStats(Health.ToString(_numbersFormatStyle), Points.ToString(_numbersFormatStyle));
    }

    void EndGame()
    {
        PauseGame(true);
        _userInterface.ShowEndgameMenu(Points.ToString(_numbersFormatStyle));
        _solidObjectBehaviour.DestroyAll();
        Points = 0;
        Health = _playerMaxHealth;
    }
    void PlayerFire()
    {
        _solidObjectBehaviour.SpawnMissle(_player.transform.position, new Quaternion(), false, _playerMissleSpeed, _playerMissleDamage);
    }
    public void PauseGame(bool isPaused)
    {
        PauseEvent.Invoke(isPaused);
    }
    public void SetPauseState(bool isPaused)
    {
        _isPaused = isPaused;
    }
}
public enum EnemyType : int
{
    Light = 0,
    Medium = 1,
    Heavy = 2
}

public class UnityEvent_bool : UnityEvent<bool>
{
   
}