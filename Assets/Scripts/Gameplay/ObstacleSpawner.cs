﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner
{
    private float _asteroidSpawnTime = 2f;
    private float _asteroidSpawnTimeElapsed = 0;
    private float _asteroidMaxXSpeed = 14f;
    private float _asteroidMaxYSpeed = 7f;
    private float _asteroidBaseYSpeed = 4f;

    private float _enemySpawnTime = 2f;
    private float _enemySpawnTimeElapsed = 0;

    private float _screenBoundsX;
    private float _screenBoundsY;
    private readonly int _numberOfEnemyTypes = Enum.GetValues(typeof(EnemyType)).Length;

    private SolidObjectBehaviour _solidObjectManager;
    public ObstacleSpawner(Vector3 screenBounds, ref SolidObjectBehaviour solidObjectManager)
    {
        _screenBoundsX = screenBounds.x;
        _screenBoundsY = screenBounds.y;
        _solidObjectManager = solidObjectManager;
    }
    public void Update()
    {
        float time = Time.deltaTime;

        _asteroidSpawnTimeElapsed += time;

        if (_asteroidSpawnTimeElapsed >= _asteroidSpawnTime)
        {
            _asteroidSpawnTimeElapsed = 0;
            float asteroidXposition = GetRandomXposition();
            float asteroidYposition = _screenBoundsY;
            Vector2 asteroidSpeed = GetRandomAsteroidSpeed();
            float asteroidSize = GetRandomAsteroidSize();
            _solidObjectManager.SpawnAsteroid(new Vector3(asteroidXposition, asteroidYposition, 0), asteroidSpeed, asteroidSize);
        }

        _enemySpawnTimeElapsed += time;

        if (_enemySpawnTimeElapsed >= _enemySpawnTime)
        {
            _enemySpawnTimeElapsed = 0;
            float enemyXposition = GetRandomXposition();
            float enemyYposition = _screenBoundsY;
            EnemyType spawnType;

            spawnType = GetRandomEnemyType();
            _solidObjectManager.SpawnEnemy(new Vector3(enemyXposition, enemyYposition, 0), spawnType);
        }
    }
    private EnemyType GetRandomEnemyType()
    {      
        return (EnemyType)Enum.ToObject(typeof(EnemyType), UnityEngine.Random.Range(0, _numberOfEnemyTypes));
    }

    private static float GetRandomAsteroidSize()
    {
        return 1 + UnityEngine.Random.value * 2;
    }
    private float GetRandomXposition()
    {
        return (_screenBoundsX * UnityEngine.Random.value) - _screenBoundsX / 2;
    }
    private Vector2 GetRandomAsteroidSpeed()
    {
        return new Vector2((UnityEngine.Random.value - 0.5f) * _asteroidMaxXSpeed * 2, -(_asteroidBaseYSpeed + UnityEngine.Random.value * _asteroidMaxYSpeed));
    }
}
